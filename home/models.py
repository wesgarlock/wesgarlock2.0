from __future__ import unicode_literals

from django.db import models

from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel
from portfolio.models import PortfolioPage
from django.shortcuts import get_object_or_404
from random import shuffle


class HomePage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
    ]

    def get_context(self, request):
        context = super(HomePage, self).get_context(request)
        portfoliopages = PortfolioPage.objects.all().order_by('-first_published_at')
        portfoliopageimages = []
        for i in portfoliopages:
            portfoliopageimages.append(i.gallery_images.first())
        # Add extra variables and return the updated context
        shuffle(portfoliopageimages)
        context['portfolio'] = portfoliopageimages
        return context
