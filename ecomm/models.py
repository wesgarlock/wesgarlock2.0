from __future__ import unicode_literals

from django.db import models
from django.urls import reverse
from django import forms

from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase
from django.urls import reverse
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index
from wagtail.wagtailsnippets.models import register_snippet
from wagtail.contrib.settings.models import BaseSetting, register_setting

# Create your models here.
class Product(Page):
    productAt = models.URLField()
    sku = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    description = models.CharField(max_length=255)
    categories = ParentalManyToManyField('ecomm.EcommCategory', blank=True)

    def main_image(self):
        gallery_item = self.gallery_images.first()
        if gallery_item:
            return gallery_item.image
        else:
            return None

    content_panels = Page.content_panels + [
        FieldPanel('sku'),
        FieldPanel('productAt'),
        FieldPanel('name'),
        FieldPanel('price'),
        FieldPanel('description'),
        FieldPanel('categories', widget=forms.CheckboxSelectMultiple),
        InlinePanel('gallery_images', label="Gallery images"),
    ]

class EcommHomePage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full")
    ]

    def get_context(self, request):
        # Update context to include only published posts, ordered by reverse-chron
        context = super(EcommHomePage, self).get_context(request)
        ecommcategory = EcommCategory.objects.all()
        context['ecommcategory'] = ecommcategory
        return context

@register_setting
class SnipcartSettings(BaseSetting):
    ApiKey = models.CharField(
        max_length=255,
        help_text='ST_YTZjZjc0ZjgtZGM1Ny00ODM3LWE5MWItOTk5ZWU5ZDBjMWNiNjM2MzY3MjI2NjM0NTU3NTM3'
    )

@register_snippet
class EcommCategory(models.Model):
    name = models.CharField(max_length=255)
    icon = models.ForeignKey(
        'wagtailimages.Image', null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )

    panels = [
        FieldPanel('name'),
        ImageChooserPanel('icon'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'ecomm categories'

class ProductGalleryImage(Orderable):
    page = ParentalKey(Product, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]
