# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from random import shuffle

from django.db import models
from random import shuffle
# Create your models here.
# -*- coding: utf-8 -*-


from django.db import models
from django import forms

from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index
from wagtail.wagtailsnippets.models import register_snippet
from django import template


# Keep the definition of PortfolioIndexPage, and add:

class PortfolioPageTag(TaggedItemBase):
    content_object = ParentalKey('PortfolioPage', related_name='tagged_items')

class PortfolioPage(Page):
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    tags = ClusterTaggableManager(through=PortfolioPageTag, blank=True)
    categories = ParentalManyToManyField('portfolio.PortfolioCategory', blank=True)

    def main_image(self):
        gallery_item = self.gallery_images.first()
        if gallery_item:
            return gallery_item.image
        else:
            return None

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('date'),
            FieldPanel('tags'),
            FieldPanel('categories', widget=forms.CheckboxSelectMultiple),
        ], heading="Portfolio information"),
        FieldPanel('intro'),
        FieldPanel('body'),
        InlinePanel('gallery_images', label="Gallery images"),
    ]


class PortfolioPageGalleryImage(Orderable):
    page = ParentalKey(PortfolioPage, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]

class PortfolioIndexPage(Page):
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]

    def get_context(self, request):
        # Update context to include only published posts, ordered by reverse-chron
        context = super(PortfolioIndexPage, self).get_context(request)
        portfoliopages = self.get_children().live().order_by('-first_published_at')
        portcategory = PortfolioCategory.objects.all()
        portfoliopages=list(portfoliopages)
        shuffle(portfoliopages)
        context['portfoliopages'] = portfoliopages
        context['portfoliocategory'] = portcategory
        return context

class PortfolioTagIndexPage(Page):

    def get_context(self, request):

        # Filter by tag
        tag = request.GET.get('tag')
        portfoliopages = PortfolioPage.objects.filter(tags__name=tag)

        # Update template context
        context = super(PortfolioTagIndexPage, self).get_context(request)
        context['portfoliopages'] = portfoliopages
        return context

@register_snippet
class PortfolioCategory(models.Model):
    name = models.CharField(max_length=255)
    icon = models.ForeignKey(
        'wagtailimages.Image', null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )

    panels = [
        FieldPanel('name'),
        ImageChooserPanel('icon'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'portfolio categories'

register = template.Library()
