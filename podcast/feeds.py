from podcast.models import PodcastPage
from django.utils.feedgenerator import Rss201rev2Feed
from django.contrib.syndication.views import Feed
import datetime
from django.db.models import permalink

#class LatestEntriesFeed(Feed):
#    title = "Police beat site news"
#    link = "/sitenews/"
#    description = "Updates on changes and additions to police beat central."
#
#    def items(self):
#       return PodcastPage.objects.order_by('date')[:5]
#
#    #def item_title(self, item):
#        return item.title

class iTunesPodcastsFeedGenerator(Rss201rev2Feed):

  def rss_attributes(self):
    return {u"version": self._version, u"xmlns:atom": u"http://www.w3.org/2005/Atom", u'xmlns:itunes': u'http://www.itunes.com/dtds/podcast-1.0.dtd'}

  def add_root_elements(self, handler):
    super(iTunesPodcastsFeedGenerator, self).add_root_elements(handler)
    handler.addQuickElement(u'itunes:subtitle', self.feed['subtitle'])
    handler.addQuickElement(u'itunes:author', self.feed['author_name'])
    handler.addQuickElement(u'itunes:summary', self.feed['description'])
    handler.addQuickElement(u'itunes:explicit', self.feed['iTunes_explicit'])
    handler.startElement(u"itunes:owner", {})
    handler.addQuickElement(u'itunes:name', self.feed['iTunes_name'])
    handler.addQuickElement(u'itunes:email', self.feed['iTunes_email'])
    handler.endElement(u"itunes:owner")
    handler.addQuickElement(u'itunes:image', self.feed['iTunes_image_url'])

  def add_item_elements(self,  handler, item):
    super(iTunesPodcastsFeedGenerator, self).add_item_elements(handler, item)
    handler.addQuickElement(u'iTunes:summary',item['summary'])
    handler.addQuickElement(u'iTunes:duration',item['duration'])
    handler.addQuickElement(u'iTunes:explicit',item['explicit'])

class iTunesPodcastPost():
  def __init__(self, podcast):
    self.id = podcast.id
    self.approval_date_time = podcast.first_published_at
    self.title = podcast.title
    self.summary = podcast.summary
    self.enclosure_url = podcast.podcast.url
    self.enclosure_length = podcast.podcast_length
    self.enclosure_mime_type = u'audio/mpeg'
    self.duration = podcast.duration
    self.explicit = u'no'

  def __unicode__(self):
    return "Podcast: %s" % self.title


  def get_absolute_url(self):
    return "podcast/%s/" %(PodcastPage.slug)

class iTunesPodcastsFeed(Feed):
  """
  A feed of podcasts for iTunes and other compatible podcatchers.
  """
  title = "My Subjects"
  link = "/podcast/"
  author_name = 'Wesley Garlock'
  description = "A Podcast of great things."
  subtitle = "From now until forever"
  summary = "People get around and chat about stuff.  You listen."
  iTunes_name = u'Wesley Garlock'
  iTunes_email = u'wesley.g.garlock@gmail.com'
  iTunes_image_url = u'href="https://s3.us-east-2.amazonaws.com/wesgarlockphotography/podcasts/Podcast+Artwork.jpg"'
  iTunes_explicit = u'no'
  feed_type = iTunesPodcastsFeedGenerator
  feed_copyright = "Copyright %s by Wesley Garlock." % datetime.date.today().year

  def items(self):
    """
    Returns a list of items to publish in this feed.
    """
    stop = datetime.datetime.now()
    start = stop - datetime.timedelta(hours=72)
    p = PodcastPage.objects.live()
    posts = []
    for x in p:
        posts.append(x)
    posts = [iTunesPodcastPost(item) for item in posts]
    return posts

  def feed_extra_kwargs(self, obj):
    extra = {}
    extra['iTunes_name'] = self.iTunes_name
    extra['iTunes_email'] = self.iTunes_email
    extra['iTunes_image_url'] = self.iTunes_image_url
    extra['iTunes_explicit'] = self.iTunes_explicit
    return extra

  def item_extra_kwargs(self, item):
    return {'summary':item.summary, 'duration':item.duration, 'explicit':item.explicit}

  def item_pubdate(self, item):
    return item.approval_date_time

  def item_enclosure_url(self, item):
    return item.enclosure_url

  def item_enclosure_length(self, item):
    return item.enclosure_length

  def item_enclosure_mime_type(self, item):
    return item.enclosure_mime_type

  def item_description(self, item):
    return item.summary
