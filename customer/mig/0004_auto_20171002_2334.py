# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-02 23:34
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0040_page_draft_title'),
        ('customer', '0003_auto_20171002_1411'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerFolderPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('date', models.DateField(verbose_name='Post date')),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.RenameModel(
            old_name='CustomerPageGalleryImage',
            new_name='CustomerFolderPageGalleryImage',
        ),
        migrations.AlterField(
            model_name='customerfolderpagegalleryimage',
            name='page',
            field=modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='gallery_images', to='customer.CustomerFolderPage'),
        ),
    ]
