# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django import forms
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index
from wagtail.wagtailsnippets.models import register_snippet
from ecomm.models import Product
from wagtail.contrib.settings.models import BaseSetting, register_setting
import zipfile
from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from wagtail.wagtailimages.models import Rendition


# Keep the definition of CustomerIndexPage, and add:

class CustomerPageTag(TaggedItemBase):
    content_object = ParentalKey('CustomerPage', related_name='tagged_items')

class CustomerFolderPage(Page):
    date          = models.DateField("Post date")
    low_resolution_file = models.FileField(upload_to='low-clientzips', default="empty.html")
    med_resolution_file = models.FileField(upload_to='med-clientzips', default="empty.html")
    high_resolution_file = models.FileField(upload_to='high-clientzips', default="empty.html")

    def main_image(self):
        gallery_item = self.gallery_images.first()
        if gallery_item:
            return gallery_item.image
        else:
            return None

    search_fields = Page.search_fields + [
        index.SearchField('title'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('low_resolution_file'),
        FieldPanel('med_resolution_file'),
        FieldPanel('high_resolution_file'),
        InlinePanel('gallery_images', label="Gallery images"),
    ]











class CustomerPage(Page):
    date          = models.DateField("Post date")
    company_name  = models.CharField(max_length=250)
    email         = models.EmailField(max_length=250, default='example@example.com')
    first_name    = models.CharField(max_length=250)
    last_name     = models.CharField(max_length=250)
    phone_number  = models.CharField(max_length=250)
    body          = RichTextField(blank=True)
    tags          = ClusterTaggableManager(through=CustomerPageTag, blank=True)
    categories    = ParentalManyToManyField('customer.CustomerCategory', blank=True)

    search_fields = Page.search_fields + [
        index.SearchField('company_name'),
        index.SearchField('body'),
        index.SearchField('title'),
        index.SearchField('phone_number'),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('first_name'),
            FieldPanel('last_name'),
            FieldPanel('phone_number'),
            FieldPanel('email'),
            FieldPanel('date'),
            FieldPanel('tags'),
            FieldPanel('categories', widget=forms.CheckboxSelectMultiple),
        ], heading="Customer information"),
        FieldPanel('company_name'),
        FieldPanel('body'),
    ]

    def get_context(self, request):
        # Update context to include only published posts, ordered by reverse-chron

        context = super(CustomerPage, self).get_context(request)
        customer_folder = self.get_children().live().order_by('-first_published_at')



        context['customer_folder'] = customer_folder
        return context


class CustomerFolderPageGalleryImage(Orderable):
    page = ParentalKey(CustomerFolderPage, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]

class CustomerIndexPage(Page):
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]

    def get_context(self, request):
        # Update context to include only published posts, ordered by reverse-chron
        context = super(CustomerIndexPage, self).get_context(request)
        customerpages = self.get_children().live().order_by('-first_published_at')
        context['customerpages'] = customerpages
        return context

class CustomerTagIndexPage(Page):

    def get_context(self, request):

        # Filter by tag
        tag = request.GET.get('tag')
        customerpages = CustomerPage.objects.filter(tags__name=tag)

        # Update template context
        context = super(CustomerTagIndexPage, self).get_context(request)
        context['customerpages'] = customerpages
        return context

@register_snippet
class CustomerCategory(models.Model):
    name = models.CharField(max_length=255)
    icon = models.ForeignKey(
        'wagtailimages.Image', null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )

    panels = [
        FieldPanel('name'),
        ImageChooserPanel('icon'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'customer categories'
